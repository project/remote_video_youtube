# Remote Video - YouTube
This module extends `remote_video` module functionality so more YouTube remote video metadata can be stored in Drupal.

## Installation
1. Install as you would normally install a contributed Drupal module. Visit [Installing contributed modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8) for further information.
2. Enable the module at `/admin/modules`.
3. Visit `/admin/config/services/remote_video` and add your YouTube Data API Key.
