<?php

namespace Drupal\remote_video_youtube\Service;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\remote_video\Services\RemoteVideoConfigServiceInterface;
use Drupal\remote_video\Services\RemoteVideoService;
use Drupal\remote_video\Services\VideoInfoResponseInterface;
use Drupal\remote_video_youtube\api\Request\GetVideosRequest;
use Drupal\remote_video_youtube\Plugin\HttpServiceApiWrapper\YouTubeV3VideosInterface;

class YouTubeRemoteVideoService extends RemoteVideoService {
    protected string $id = 'youtube';
    protected string $title = 'YouTube';
    protected string $domain = 'youtube.com';

    private YouTubeV3VideosInterface $api;

    public function __construct(RemoteVideoConfigServiceInterface $remoteVideoConfig, YouTubeV3VideosInterface $api) {
        parent::__construct($remoteVideoConfig);
        $this->api = $api;
        $this->setEditableConfigFieldDescription('api_key', new TranslatableMarkup('Click <a href="https://developers.google.com/youtube/v3/getting-started" target="_blank">here</a> for more info.'));
        $this->setEditableConfigValidationPattern('api_key', '/[a-z0-9]{21}-[a-z0-9]{17}/i');
    }

    public function isVideoUrl(string $url): bool
    {
        return (bool) $this->getVideoIdFromUrl($url);
    }

    public function getVideoIdFromUrl(string $url): ?string
    {
        $parts = parse_url($url);
        if (empty($parts['query'])) {
            return NULl;
        }
        parse_str($parts['query'], $query);
        return $query['v'] ?? NULL;
    }

    public function getApiKey(): ?string
    {
        return $this->getConfig('api_key');
    }

    public function getVideoInfo(string $url): VideoInfoResponseInterface
    {
        $request = new GetVideosRequest();
        $request->setPart('snippet,contentDetails');
        $request->setIdFromUrl($url);
        $request->setKey($this->getApiKey());
        return $this->api->getVideos($request);
    }

}
