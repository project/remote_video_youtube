<?php

namespace Drupal\remote_video_youtube\api\Response;

use DateInterval;
use DateTimeImmutable;
use Drupal\remote_video\Services\VideoInfoResponseInterface;

class GetVideosResponse implements VideoInfoResponseInterface {
    private array $response;

    public function __construct(array $response) {
        $this->response = $response;
    }

    public function getResponse() {
        return $this->response;
    }

    public function getPublishedDateTime(): \DateTime
    {
        return new \DateTime($this->response['items'][0]['snippet']['publishedAt']);
    }

    public function getTitle(): string
    {
        return $this->response['items'][0]['snippet']['title'];
    }

    public function getChannelTitle(): string
    {
        return $this->response['items'][0]['snippet']['channelTitle'];
    }

    public function getDefaultAudioLanguage(): string
    {
        return $this->response['items'][0]['snippet']['defaultAudioLanguage'];
    }

    public function getDescription(): string
    {
        return $this->response['items'][0]['snippet']['description'];
    }

    public function getDuration(): int
    {
        $reference = new DateTimeImmutable;
        $dateInterval = new DateInterval($this->response['items'][0]['contentDetails']['duration']);
        $endTime = $reference->add($dateInterval);
        return $endTime->getTimestamp() - $reference->getTimestamp();
    }

    public function getThumbnailSourceUrl(): string
    {
        return $this->response['items'][0]['snippet']['thumbnails']['maxres']['url'];
    }

    public function getId(): string
    {
        return $this->response['items'][0]['id'];
    }

    public function getNewThumbnailFilename(): string
    {
        return $this->getId() . '.' . pathinfo($this->getThumbnailSourceUrl(), PATHINFO_EXTENSION);
    }
}
