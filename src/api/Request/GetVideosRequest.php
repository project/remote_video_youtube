<?php

namespace Drupal\remote_video_youtube\api\Request;

use Drupal\http_client_manager\Request\HttpRequestBase;
use Drupal\remote_video_youtube\api\Commands\TvServicesContents;
use Drupal\remote_video_youtube\api\Parameters\GetVideosRequestParameters as Param;

/**
 * Class GetVideosRequest.
 *
 * @package Drupal\remote_video_youtube\api\Request
 */
class GetVideosRequest extends HttpRequestBase {

    protected string $id;
    protected string $key;
    protected string $part;

    public function getId() {
        return $this->id;
    }

    public function setIdFromUrl($url) {
        $parts = parse_url($url);
        if (isset($parts['query'])) {
            parse_str($parts['query'], $query);
            if (isset($query['v'])) {
                $this->setId($query['v']);
            }
        }
        return $this;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getPart() {
        return $this->part;
    }

    public function setPart($part) {
        $this->part = $part;
        return $this;
    }

    public function getKey() {
        return $this->key;
    }

    public function setKey($key) {
        $this->key = $key;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCommand() {
        return TvServicesContents::GET_VIDEOS;
    }

    /**
     * {@inheritdoc}
     */
    public function getArgs() {
        return [
            Param::API_KEY => $this->getKey(),
            Param::VIDEO_ID => $this->getId(),
            Param::VIDEO_PART => $this->getPart(),
        ];
    }

}
