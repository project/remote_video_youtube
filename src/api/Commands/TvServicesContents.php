<?php

namespace Drupal\remote_video_youtube\api\Commands;

/**
 * Class TvServicesContents.
 *
 * @package Drupal\remote_video_youtube\api\Commands
 */
final class TvServicesContents {

    const GET_VIDEOS = 'GetVideos';

}
