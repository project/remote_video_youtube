<?php

namespace Drupal\remote_video_youtube\api\Parameters;

/**
 * Class TvServicesContents.
 *
 * @package Drupal\remote_video_youtube\api\Parameters
 */
final class GetVideosRequestParameters {

    const VIDEO_ID = 'id';

    const API_KEY = 'key';

    const VIDEO_PART = 'part';

}
