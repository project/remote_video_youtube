<?php

namespace Drupal\remote_video_youtube\Plugin\HttpServiceApiWrapper;

use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\http_client_manager\Plugin\HttpServiceApiWrapper\HttpServiceApiWrapperBase;
use Drupal\remote_video_youtube\api\Request\GetVideosRequest;
use Drupal\remote_video_youtube\api\Response\GetVideosResponse;
use GuzzleHttp\Command\Exception\CommandException;

/**
 * Class YouTubeV3Videos.
 *
 * @package Drupal\remote_video\Plugin\HttpServiceApiWrapper
 */
class YouTubeV3Videos extends HttpServiceApiWrapperBase implements YouTubeV3VideosInterface {

    use LoggerChannelTrait;

    /**
     * {@inheritdoc}
     */
    public function getHttpClient() {
        return $this->httpClientFactory->get(self::SERVICE_API);
    }

    /**
     * {@inheritdoc}
     */
    public function GetVideos(GetVideosRequest $request): GetVideosResponse
    {
        return new GetVideosResponse($this->callByRequest($request));
    }

    /**
     * {@inheritdoc}
     */
    protected function logError(CommandException $e) {
        $this->getLogger(self::SERVICE_API)->debug($e->getMessage());
    }

}
