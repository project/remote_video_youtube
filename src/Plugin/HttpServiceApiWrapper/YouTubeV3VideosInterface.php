<?php

namespace Drupal\remote_video_youtube\Plugin\HttpServiceApiWrapper;

use Drupal\remote_video\Services\VideoInfoResponseInterface;
use Drupal\remote_video_youtube\api\Request\GetVideosRequest;

/**
 * Interface YouTubeV3VideosInterface.
 *
 * @package Drupal\remote_video_youtube\Plugin\HttpServiceApiWrapper
 */
interface YouTubeV3VideosInterface {

    const SERVICE_API = 'remote_video_youtube.youtube_v3_videos_api';

    /**
     * Get Videos.
     *
     * @param GetVideosRequest $request
     *   The HTTP Request object.
     *
     * @return VideoInfoResponseInterface
     *   The service response.
     */
    public function getVideos(GetVideosRequest $request): VideoInfoResponseInterface;

}
